# circular_buffer library



**Author**: Hubert Melchert

**Email**: hubert.melchert@gmail.com

Library for handling multiple circular buffers.
Data size is fixed to single bytes.

Module itself doesn't allocate memory. Instead user has to provide pointer to allocated memory for buffer data.
Library's goal is to encapsulate container as much as possible so there is no acces to head or tail variables from outside.
That requirement causes allocation problems as size of circBuffer_t cannot be statically obtained outside the module. To avoid using
dynamic allocation in simple embedded applications total amount of buffers has to be defined at compilation time using **#define MAX_BUFFERS_COUNT** in circ.h

# Testing

Unit-testing is based on Ceedling tool. To run tests simply call:

```ceedling test:all```

If ceedling is not installed in your environment follow these steps:

1. Install Ruby (if not installed already)

    for example

    ```pacman -S ruby```

    or

    ```apt-get install ruby```

2. Use Ruby package manager Gem to install Ceedling

    ```gem install ceedling```
    
3. Add ceedling to PATH
    
    for example add line
    
    ```export PATH=$PATH:~/.gem/ruby/2.4.0/bin```
    
    to ~/.bashrc in MSYS2

Repository provides rather self-explanatory example in example.c file. Run make with no arguments and execute example.
