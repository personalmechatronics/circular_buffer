#include "circ.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>


#ifndef NELEMS
#define NELEMS(array) (sizeof(array) / sizeof(array[0]))
#endif

#ifndef ASSERT
#define CONCAT(a,b) a##b
#define __ASSERT(predicate, line) \
            typedef char CONCAT(assertion_failed, line)[2*!!(predicate)-1];
#define ASSERT(predicate) __ASSERT(predicate, __LINE__)
#endif


struct circ
{
    struct
    {
        size_t head;
        size_t tail;
        size_t size;
        bool full;
    }info;
    uint8_t *data;
};

static volatile struct circ buffers[MAX_BUFFERS_COUNT];

//+------------------+
//| PUBLIC FUNCTIONS |
//+==================+

size_t CIRC_getSpaceLeft(const circBuffer_t buffer)
{
    if(buffer == NULL)
    {
        return 0;
    }

    return buffer->info.size - CIRC_getNumberOfBytesInBuffer(buffer);
}

size_t CIRC_getNumberOfBytesInBuffer(const circBuffer_t buffer)
{
    if(buffer == NULL)
    {
        return 0;
    }

    size_t head = buffer->info.head;

    if(buffer->info.full)
    {
        return buffer->info.size;
    }
    else
    {
        return (head >= buffer->info.tail) ?
               (head - buffer->info.tail)
               : (head + (buffer->info.size - buffer->info.tail));
    }
}

circBuffer_t CIRC_init(uint8_t *const dataSpace, size_t dataSize)
{
    if(dataSpace == NULL || dataSize == 0)
    {
        return NULL;
    }

    volatile struct circ *buff = buffers;

    volatile struct circ *newBuff = NULL;

    for(size_t i = 0; i < NELEMS(buffers); i++, buff++)
    {
        if(buff->info.size > 0 && buff->data == dataSpace)
        {
            return NULL;
        }

        if(buff->info.size == 0)
        {
            newBuff = buff;
            break;
        }
    }

    if(newBuff == NULL)
    {
        return NULL;
    }

    newBuff->info.size = dataSize;
    newBuff->info.head = buff->info.tail = 0;
    newBuff->info.full = false;
    newBuff->data = dataSpace;
    return newBuff;
}

size_t CIRC_read(circBuffer_t buffer, uint8_t *dest, size_t size)
{
    if(buffer == NULL || dest == NULL || size == 0)
    {
        return 0;
    }

    size_t bytesAvailable = CIRC_getNumberOfBytesInBuffer(buffer);

    if(bytesAvailable == 0)
    {
        return 0;
    }

    if(size == 1)
    {
        *dest = buffer->data[buffer->info.tail];
        buffer->info.tail = (buffer->info.tail + 1) % buffer->info.size;
        buffer->info.full = false;
        return 1;
    }

    if(size > bytesAvailable)
    {
        size = bytesAvailable;
    }

    size_t tailAfterRead = buffer->info.tail + size;
    size_t firstChunkSize = size;

    if(tailAfterRead >= buffer->info.size)
    {
        firstChunkSize = buffer->info.size - buffer->info.tail;
        tailAfterRead = 0;
    }

    memcpy(dest, &buffer->data[buffer->info.tail], firstChunkSize);
    buffer->info.tail = tailAfterRead;
    size_t secondChunkSize = size - firstChunkSize;

    if(secondChunkSize)
    {
        memcpy(&dest[firstChunkSize], &buffer->data[buffer->info.tail],
               secondChunkSize);
        buffer->info.tail = secondChunkSize;
    }

    buffer->info.full = false;
    return size;
}

size_t CIRC_write(circBuffer_t buffer, const uint8_t *src, size_t size)
{
    if(buffer == NULL || src == NULL || size == 0 || buffer->info.full)
    {
        return 0;
    }

    size_t spaceLeft = CIRC_getSpaceLeft(buffer);

    if(size >= spaceLeft)
    {
        size = spaceLeft;
        buffer->info.full = true;
    }

    if(size == 1)
    {
        buffer->data[buffer->info.head] = *src;
        buffer->info.head = (buffer->info.head + 1) % buffer->info.size;
        return 1;
    }

    size_t headAfterWrite = buffer->info.head + size;
    size_t firstChunkSize = size;

    if(headAfterWrite >= buffer->info.size)
    {
        firstChunkSize = buffer->info.size - buffer->info.head;
        headAfterWrite = 0;
    }

    memcpy(&buffer->data[buffer->info.head], src, firstChunkSize);
    buffer->info.head = headAfterWrite;
    size_t secondChunkSize = size - firstChunkSize;

    if(secondChunkSize)
    {
        memcpy(&buffer->data[buffer->info.head], &src[firstChunkSize],
               secondChunkSize);
        buffer->info.head = secondChunkSize;
    }

    return size;
}

size_t CIRC_getSize(const circBuffer_t buffer)
{
    if(buffer == NULL)
    {
        return 0;
    }

    return buffer->info.size;
}

bool CIRC_isFull(const circBuffer_t buffer)
{
    if(buffer == NULL)
    {
        return 0;
    }

    return buffer->info.full == true;
}


//+---------------------+
//| ONLY FOR UNIT TESTS |
//+=====================+

#ifdef TEST
void CIRC_deinit(void)
{
    memset((void *)buffers, 0, sizeof(buffers));
}
#endif

//+-------------------+
//| PRIVATE FUNCTIONS |
//+===================+

