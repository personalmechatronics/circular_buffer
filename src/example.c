#include "circ.h"

#include <stdio.h>
#include <stdint.h>

#define UNUSED(a) ((void)a)

#define NELEMS(array) (sizeof(array) / sizeof(array[0]))

static void print_array(uint8_t *array, size_t nelems)
{
    for(size_t i = 0; i < nelems; i++)
    {
        printf("%d ", array[i]);
    }
    printf("\n");
}

int main(void)
{
    uint8_t buffData[20];
    //init 
    circBuffer_t buffer = CIRC_init(buffData, sizeof(buffData));

    //write
    uint8_t writeData[] = {1, 2, 3, 4};
    printf("Example: writing %zu bytes to buffer of size %zu\n", 
            sizeof(writeData), CIRC_getSize(buffer));
    size_t writeSize = CIRC_write(buffer, writeData, sizeof(writeData));
    printf("Wrote %zu bytes\n", writeSize);

    //read
    uint8_t readData[sizeof(writeData)];
    size_t readSize = CIRC_read(buffer, readData, sizeof(readData));
    printf("Read %zu bytes:\n", readSize);
    print_array(readData, readSize);
    return 0;
}
