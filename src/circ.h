#ifndef CIRC_H
#define CIRC_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

//+-------------+
//| USER CONFIG |
//+=============+

#define MAX_BUFFERS_COUNT 4

//+-----------+
//| INTERFACE |
//+===========+
typedef volatile struct circ *circBuffer_t;

circBuffer_t CIRC_init(uint8_t *const dataSpace, size_t dataSize);
size_t CIRC_write(circBuffer_t buffer, const uint8_t *src, size_t size);
size_t CIRC_read(circBuffer_t buffer, uint8_t *dest, size_t size);
size_t CIRC_getSpaceLeft(const circBuffer_t buffer);
size_t CIRC_getNumberOfBytesInBuffer(const circBuffer_t buffer);
size_t CIRC_getSize(const circBuffer_t buffer);
bool CIRC_isFull(const circBuffer_t buffer);

//+---------------------+
//| ONLY FOR UNIT TESTS |
//+=====================+

#ifdef TEST
void CIRC_deinit(void);
#endif
#endif //CIRC_H
