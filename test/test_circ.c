#include "unity.h"
#include "circ.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define NELEMS(array) (sizeof(array) / sizeof(array[0]))


// cppcheck-suppress unusedFunction 
void test_CIRC_init_PassingNull_should_ReturnNull(void)
{
    TEST_ASSERT_NULL(CIRC_init(NULL, 20));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_init_PassingZeroSize_should_ReturnNull(void)
{
    uint8_t buff[2];
    TEST_ASSERT_NULL(CIRC_init(buff, 0));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_init_PassingNullAndZeroSize_should_ReturnNull(void)
{
    TEST_ASSERT_NULL(CIRC_init(NULL, 0));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_init_InitingTooMany_should_ReturnNULL(void)
{
    uint8_t buff[MAX_BUFFERS_COUNT + 2];

    for(size_t i = 0; i < MAX_BUFFERS_COUNT; i++)
    {
        TEST_ASSERT_NOT_NULL(CIRC_init(&buff[i], 1));
    }
    TEST_ASSERT_NULL(CIRC_init(&buff[MAX_BUFFERS_COUNT], 1));
    TEST_ASSERT_NULL(CIRC_init(&buff[MAX_BUFFERS_COUNT + 1], 1));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_init_InitingTwiceOnSameSpace_should_ReturnNULLSecondTime(void)
{
    uint8_t buff[2];
    TEST_ASSERT_NOT_NULL(CIRC_init(buff, sizeof(buff)));
    TEST_ASSERT_NULL(CIRC_init(buff, sizeof(buff)));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_write_PassingNullAsData_should_ReturnZeroSize(void)
{
    uint8_t rawData[20];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    
    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(0, CIRC_write(circ, NULL, 2));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_write_PassingNullAsBuffer_should_ReturnZeroSize(void)
{
    uint8_t rawData[20];

    TEST_ASSERT_EQUAL(0, CIRC_write(NULL, rawData, 2));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_write_WritingZeroBytes_should_ReturnZeroSize(void)
{
    uint8_t rawData[20];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);

    uint8_t testData[] = {1, 2, 3}; 
    TEST_ASSERT_EQUAL(0, CIRC_write(circ, testData, 0));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_write_WritingOneByteToEmptyBuff_should_StoreDataAndReturnSize1(void)
{
    uint8_t rawData[10];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));

    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_getSpaceLeft(circ));
    
    uint8_t testData = 0xAA; 
    TEST_ASSERT_EQUAL(1, CIRC_write(circ, &testData, 1));
    uint8_t *storedDataStart = rawData;
    TEST_ASSERT_EQUAL(testData, *storedDataStart);
}

// cppcheck-suppress unusedFunction 
void test_CIRC_write_WritingData_should_NotModifySource(void)
{
    uint8_t rawData[10];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));

    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_getSpaceLeft(circ));
    
    const uint8_t testData[] = {1, 2, 3};
    uint8_t src[sizeof(testData)];
    memcpy(src, testData, sizeof(src));
    TEST_ASSERT_EQUAL(sizeof(src), CIRC_write(circ, src, sizeof(src)));
    TEST_ASSERT_EQUAL_MEMORY((uint8_t *)testData, src, sizeof(src));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_write_Writing10BytesToEmptyBuffer_should_ReturnSize10AndStoreData(void)
{
    uint8_t rawData[20];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));

    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_getSpaceLeft(circ));
    
    uint8_t testData[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_write(circ, testData, sizeof(testData)));
    uint8_t *dataInBuffer = rawData;
    TEST_ASSERT_EQUAL_MEMORY((uint8_t *)testData, dataInBuffer, sizeof(testData));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_write_Writing5BytesTwiceToEmptyBuffer_should_ReturnSize5AndStoreData(void)
{
    uint8_t rawData[20];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));

    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_getSpaceLeft(circ));

    uint8_t testData[] = {1, 2, 3, 4, 5};
    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_write(circ, testData, sizeof(testData)));
    uint8_t *dataInBuffer = rawData;
    TEST_ASSERT_EQUAL_MEMORY((uint8_t *)testData, dataInBuffer, sizeof(testData));

    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_write(circ, testData, sizeof(testData)));
    dataInBuffer += sizeof(testData);
    TEST_ASSERT_EQUAL_MEMORY((uint8_t *)testData, dataInBuffer, sizeof(testData));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_write_WritingTooManyBytes_should_ReturnSizeOfBufferAndStoreAllowedData(void)
{
    uint8_t rawData[5];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));

    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_getSpaceLeft(circ));

    uint8_t testData[] = {1, 2, 3, 4, 5, 6, 7};
    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_write(circ, testData, sizeof(testData)));
    uint8_t *dataInBuffer = rawData;
    TEST_ASSERT_EQUAL_MEMORY((uint8_t *)testData, dataInBuffer, sizeof(rawData));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_write_WritingOverEndOfBuffer(void)
{
    uint8_t src[] = {1, 2, 3, 4, 5, 6, 7};
    uint8_t rawData[5];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);

    uint8_t *dataInBuffer = rawData;
    uint8_t firstChunkSize = 3;
    size_t secondChunkSize = sizeof(src) - firstChunkSize;
    uint8_t dummyRead[firstChunkSize];
    TEST_ASSERT(firstChunkSize < sizeof(rawData)); 
    TEST_ASSERT_EQUAL(firstChunkSize, CIRC_write(circ, src, firstChunkSize));
    TEST_ASSERT_EQUAL(firstChunkSize, CIRC_read(circ, dummyRead, sizeof(dummyRead)));
    //writing over end of buffer
    TEST_ASSERT_EQUAL(secondChunkSize, CIRC_write(circ, &src[firstChunkSize],
                      secondChunkSize));
    uint8_t expectedMem[sizeof(rawData)] = {6, 7, 3, 4, 5};
    TEST_ASSERT_EQUAL_MEMORY(expectedMem, dataInBuffer, sizeof(expectedMem));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_write_WritingToFullBuffer_should_ReturnZero(void)
{
    uint8_t src[] = {1, 2, 3, 4, 5};
    uint8_t rawData[5];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);

    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_write(circ, src, sizeof(src)));
    //bufor powinien byc juz pelen
    TEST_ASSERT_EQUAL(0, CIRC_getSpaceLeft(circ));
    TEST_ASSERT_EQUAL(0, CIRC_write(circ, src, 1));
}
// cppcheck-suppress unusedFunction 
void test_CIRC_write_WritingSingleBytesOverEndOfBuffer(void)
{
    uint8_t src[] = {1, 2, 3, 4, 5, 6, 7};
    uint8_t rawData[5];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    uint8_t *dataInBuffer = rawData;
    size_t firstChunkSize = 3;
    size_t secondChunkSize = sizeof(src) - firstChunkSize;
    uint8_t dummyRead[firstChunkSize];
    TEST_ASSERT(firstChunkSize < sizeof(rawData)); 

    for(uint8_t i = 0; i < firstChunkSize; i++)
    {
        TEST_ASSERT_EQUAL(1, CIRC_write(circ, &src[i], 1));
    }

    TEST_ASSERT_EQUAL(firstChunkSize, CIRC_read(circ, dummyRead, firstChunkSize));
    //writing over end of buffer
    for(uint8_t i = 0; i < secondChunkSize; i++)
    {
        TEST_ASSERT_EQUAL(1, CIRC_write(circ, &src[firstChunkSize + i], 1));
    }
    uint8_t expectedMem[sizeof(rawData)] = {6, 7, 3, 4, 5};
    TEST_ASSERT_EQUAL_MEMORY(expectedMem, dataInBuffer, sizeof(expectedMem));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_write_Writing100000_should_Store100000Bytes(void)
{
    uint8_t rawData[100000];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));

    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_getSpaceLeft(circ));
    
    uint8_t testData[sizeof(rawData)];
    for(size_t i = 0; i < sizeof(testData); i++)
    {
        testData[i] = (uint8_t)i;
    }
    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_write(circ, testData, sizeof(testData)));
    uint8_t *dataInBuffer = rawData;
    TEST_ASSERT_EQUAL_MEMORY(testData, dataInBuffer, sizeof(testData));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_write_Writing100000BytesOverEndOfBuffer_should_Store100000Bytes(void)
{
    uint8_t rawData[100000];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));

    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_getSpaceLeft(circ));
    
    uint8_t testData[sizeof(rawData)];
    memset(testData, 0xAA, sizeof(testData));
    size_t firstChunkSize = sizeof(testData) - 10;
    TEST_ASSERT_EQUAL(firstChunkSize, CIRC_write(circ, testData, firstChunkSize));
    uint8_t dummyRead[firstChunkSize];
    TEST_ASSERT_EQUAL(firstChunkSize, CIRC_read(circ, dummyRead, firstChunkSize));

    for(size_t i = 0; i < sizeof(testData); i++)
    {
        testData[i] = (uint8_t)i;
    }

    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_write(circ, testData, sizeof(testData)));
    uint8_t *dataInBuffer = rawData;
    TEST_ASSERT_EQUAL_MEMORY(testData, &dataInBuffer[firstChunkSize], sizeof(testData) - firstChunkSize);
    TEST_ASSERT_EQUAL_MEMORY(&testData[sizeof(testData) - firstChunkSize], dataInBuffer, firstChunkSize);
}

// cppcheck-suppress unusedFunction 
void test_CIRC_getSpaceLeft_EmptyBuffer_should_ReturnBufferSize(void)
{
    uint8_t rawData[5];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));

    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_getSpaceLeft(circ));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_getSpaceLeft_NullBuffer_should_ReturnZero(void)
{
    TEST_ASSERT_EQUAL(0, CIRC_getSpaceLeft(NULL));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_getSpaceLeft_FullBuffer_should_ReturnZero(void)
{
    uint8_t rawData[5];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);
    uint8_t testData[] = {1, 2, 3, 4, 5};
    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_write(circ, testData, sizeof(testData)));
    TEST_ASSERT_EQUAL(0, CIRC_getSpaceLeft(circ));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_getSpaceLeft_HalfFilledBuffer_should_ReturnHalfSize(void)
{
    uint8_t rawData[6];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);
    uint8_t testData[] = {1, 2, 3};
    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_write(circ, testData, sizeof(testData)));
    TEST_ASSERT_EQUAL(3, CIRC_getSpaceLeft(circ));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_getNumberOfBytesInBuffer(void)
{
    uint8_t rawData[10];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);
    uint8_t testData[] = {1, 2, 3};

    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_write(circ, testData, sizeof(testData)));
    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_getNumberOfBytesInBuffer(circ));

    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_write(circ, testData, sizeof(testData)));
    TEST_ASSERT_EQUAL(2 * sizeof(testData), CIRC_getNumberOfBytesInBuffer(circ));

    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_write(circ, testData, sizeof(testData)));
    TEST_ASSERT_EQUAL(3 * sizeof(testData), CIRC_getNumberOfBytesInBuffer(circ));

    TEST_ASSERT_EQUAL(1, CIRC_write(circ, testData, sizeof(testData)));
    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_getNumberOfBytesInBuffer(circ));

    TEST_ASSERT_EQUAL(0, CIRC_getNumberOfBytesInBuffer(NULL));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_read_PassingNullAsBuffer_should_ReturnZeroSizeAndNotModifyDst(void)
{
    uint8_t dst[] = {1, 2, 3};
    uint8_t dstCpy[sizeof(dst)];
    memcpy(dstCpy, dst, sizeof(dst));

    TEST_ASSERT_EQUAL(0, CIRC_read(NULL, dst, sizeof(dst)));
    TEST_ASSERT_EQUAL_MEMORY(dst, dstCpy, sizeof(dst));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_read_PassingNullAsDestination_should_ReturnZeroSize(void)
{
    uint8_t rawData[10];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(0, CIRC_read(circ, NULL, 2));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_read_ReadingZeroBytes_should_ReturnZeroSizeAndNotModifyDst(void)
{
    uint8_t dst[] = {1, 2, 3};
    uint8_t dstCpy[sizeof(dst)];
    memcpy(dstCpy, dst, sizeof(dst));

    uint8_t rawData[10];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));

    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(0, CIRC_read(circ, dst, 0));
    TEST_ASSERT_EQUAL_MEMORY(dst, dstCpy, sizeof(dst));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_read_ReadingOneByte_should_ReturnSizeOneAndStoreOneByteAndNotModifyDst(void)
{
    uint8_t write = 0xAA;
    uint8_t read = 0;

    uint8_t rawData[10];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_EQUAL(sizeof(write), CIRC_write(circ, &write, sizeof(write)));
    TEST_ASSERT_EQUAL(sizeof(read), CIRC_read(circ, &read, sizeof(read)));
    TEST_ASSERT_EQUAL(write, read);
}

// cppcheck-suppress unusedFunction 
void test_CIRC_read_ReadingBytesFromEmptyBuffer_should_ReturnZeroSize(void)
{
    uint8_t dst[] = {1, 2, 3};
    uint8_t dstCpy[sizeof(dst)];
    memcpy(dstCpy, dst, sizeof(dst));

    uint8_t rawData[10];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));

    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(0, CIRC_read(circ, dst, sizeof(dst)));
    TEST_ASSERT_EQUAL_MEMORY(dst, dstCpy, sizeof(dst));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_read_ReadingBytes_should_ReturnValidData(void)
{
    uint8_t src[] = {1, 2, 3};
    uint8_t dst[sizeof(src)] = {0};

    uint8_t rawData[10];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(sizeof(src), CIRC_write(circ, src, sizeof(src)));
    TEST_ASSERT_EQUAL(sizeof(src), CIRC_read(circ, dst, sizeof(dst)));
    TEST_ASSERT_EQUAL_MEMORY(src, dst, sizeof(src));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_read_Reading3BytesFromBufferWith2Bytes_should_Read2Bytes(void)
{
    uint8_t src[] = {1, 2};
    uint8_t dst[sizeof(src) + 1] = {0};

    uint8_t rawData[10];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(sizeof(src), CIRC_write(circ, src, sizeof(src)));
    TEST_ASSERT_EQUAL(sizeof(src), CIRC_read(circ, dst, sizeof(dst)));
    TEST_ASSERT_EQUAL_MEMORY(src, dst, sizeof(src));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_read_ReadingHalves_should_ReturnContinousData(void)
{
    uint8_t src[] = {1, 2, 3, 4, 5, 6};
    uint8_t dst[sizeof(src)] = {0};

    uint8_t rawData[10];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(sizeof(src), CIRC_write(circ, src, sizeof(src)));
    TEST_ASSERT_EQUAL(3, CIRC_read(circ, dst, 3));
    TEST_ASSERT_EQUAL(3, CIRC_read(circ, &dst[3], 3));
    TEST_ASSERT_EQUAL_MEMORY(src, dst, sizeof(src));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_read_ReadingChunksOverEndOfBuffer_should_ReturnContinousData(void)
{
    uint8_t src[] = {1, 2, 3, 4, 5, 6, 7};
    uint8_t dst[sizeof(src)] = {0};

    const size_t dataSize = 5;
    uint8_t rawData[dataSize];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));

    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(dataSize, CIRC_write(circ, src, dataSize));
    TEST_ASSERT_EQUAL(dataSize - 1, CIRC_read(circ, dst, dataSize - 1));
    size_t remSize = sizeof(src) - dataSize;
    TEST_ASSERT_EQUAL(remSize, CIRC_write(circ, &src[dataSize], remSize));
    //reading over end of buffer
    TEST_ASSERT_EQUAL(remSize + 1, CIRC_read(circ, &dst[dataSize - 1], remSize + 1));
    TEST_ASSERT_EQUAL_MEMORY(src, dst, sizeof(src));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_read_ReadingSingleBytesOverEnd_should_ReturnContinousData(void)
{
    uint8_t src[] = {1, 2, 3, 4, 5, 6, 7};

    uint8_t rawData[5];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);
    size_t firstChunkSize = 3;
    size_t secondChunkSize = sizeof(src) - firstChunkSize;
    TEST_ASSERT_EQUAL(firstChunkSize, CIRC_write(circ, src, firstChunkSize));

    for(uint8_t i = 0; i < firstChunkSize; i++)
    {
        uint8_t dst = 0;
        TEST_ASSERT_EQUAL(1, CIRC_read(circ, &dst, 1));
        TEST_ASSERT_EQUAL(src[i], dst);
    }

    TEST_ASSERT_EQUAL(secondChunkSize, CIRC_write(circ, &src[firstChunkSize],
                                                  secondChunkSize));
    //reading over end of buffer
    for(uint8_t i = 0; i < secondChunkSize; i++)
    {
        uint8_t dst = 0;
        TEST_ASSERT_EQUAL(1, CIRC_read(circ, &dst, 1));
        TEST_ASSERT_EQUAL(src[firstChunkSize + i], dst);
    }
}

// cppcheck-suppress unusedFunction 
void test_CIRC_read_Reading100000_should_Store100000Bytes(void)
{
    uint8_t rawData[100000];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));

    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_getSpaceLeft(circ));
    
    uint8_t testData[sizeof(rawData)];
    for(size_t i = 0; i < sizeof(testData); i++)
    {
        testData[i] = (uint8_t)i;
    }
    uint8_t readData[sizeof(testData)];
    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_write(circ, testData, sizeof(testData)));
    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_read(circ, readData, sizeof(readData)));
    TEST_ASSERT_EQUAL_MEMORY(testData, readData, sizeof(testData));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_read_Reading100000BytesOverEndOfBuffer_should_Store100000Bytes(void)
{
    uint8_t rawData[100000];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));

    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(sizeof(rawData), CIRC_getSpaceLeft(circ));
    
    uint8_t testData[sizeof(rawData)];
    memset(testData, 0xAA, sizeof(testData));
    size_t firstChunkSize = sizeof(testData) - 10;
    TEST_ASSERT_EQUAL(firstChunkSize, CIRC_write(circ, testData, firstChunkSize));
    uint8_t dummyRead[firstChunkSize];
    TEST_ASSERT_EQUAL(firstChunkSize, CIRC_read(circ, dummyRead, firstChunkSize));

    for(size_t i = 0; i < sizeof(testData); i++)
    {
        testData[i] = (uint8_t)i;
    }

    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_write(circ, testData, sizeof(testData)));
    uint8_t readData[sizeof(testData)];
    TEST_ASSERT_EQUAL(sizeof(testData), CIRC_read(circ, readData, sizeof(readData)));
    TEST_ASSERT_EQUAL_MEMORY(testData, readData, sizeof(testData));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_getSize_PassingValidBuffer_should_ReturnValidSize(void)
{
    uint8_t rawData[10];
    circBuffer_t circ = CIRC_init(rawData, 1);
    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(1, CIRC_getSize(circ));
    CIRC_deinit();

    circBuffer_t circ2 = CIRC_init(rawData, 4);
    TEST_ASSERT_NOT_NULL(circ2);
    TEST_ASSERT_EQUAL(4, CIRC_getSize(circ2));

    CIRC_deinit();
    circBuffer_t circ3 = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ3);
    TEST_ASSERT_EQUAL(10, CIRC_getSize(circ3));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_getSize_PassingNull_should_ReturnSizeZero(void)
{
    TEST_ASSERT_EQUAL(0, CIRC_getSize(NULL));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_isFull_ofEmptyBufferShouldReturnZero(void)
{
    uint8_t rawData[10];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);
    TEST_ASSERT_EQUAL(0, CIRC_isFull(circ));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_isFull_ofFullBufferShouldReturnOne(void)
{
    uint8_t rawData[5];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);
    uint8_t data[sizeof(rawData)];
    TEST_ASSERT_EQUAL(sizeof(data), CIRC_write(circ, data, sizeof(data)));
    TEST_ASSERT_EQUAL(true, CIRC_isFull(circ));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_isFull_ofAlmostFullBufferShouldReturnZero(void)
{
    uint8_t rawData[5];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);
    uint8_t data[sizeof(rawData) - 1];
    TEST_ASSERT_EQUAL(sizeof(data), CIRC_write(circ, data, sizeof(data)));
    TEST_ASSERT_EQUAL(0, CIRC_isFull(circ));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_isFull_ofFilledAndReadBufferShouldReturnZero(void)
{
    uint8_t rawData[5];
    circBuffer_t circ = CIRC_init(rawData, sizeof(rawData));
    TEST_ASSERT_NOT_NULL(circ);
    uint8_t data[sizeof(rawData)];
    TEST_ASSERT_EQUAL(sizeof(data), CIRC_write(circ, data, sizeof(data)));
    uint8_t dummy;
    TEST_ASSERT_EQUAL(1, CIRC_read(circ, &dummy, 1));
    TEST_ASSERT_EQUAL(0, CIRC_isFull(circ));
}

// cppcheck-suppress unusedFunction 
void test_CIRC_isFull_ofNullBufferShouldReturnZero(void)
{
    TEST_ASSERT_EQUAL(0, CIRC_isFull(NULL));
}

// cppcheck-suppress unusedFunction 
void suiteSetUp(void)
{
}

// cppcheck-suppress unusedFunction 
int suiteTearDown(int num_failures)
{
    (void)num_failures;
    return 0;
}

// cppcheck-suppress unusedFunction 
void setUp(void)
{
}

// cppcheck-suppress unusedFunction 
void tearDown(void)
{
    CIRC_deinit();
}
